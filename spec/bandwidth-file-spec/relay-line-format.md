<a id="bandwidth-file-spec.txt-2.3"></a>

# Relay Line format { #relay-line }

It consists of zero or more RelayLines containing relay ids and
bandwidths. The relays and their KeyValues are in arbitrary order.

There MUST NOT be multiple KeyValue pairs with the same key in the same
RelayLine. If there are, the parser SHOULD choose an arbitrary Value.

There MUST NOT be multiple RelayLines per relay identity (node_id or
master_key_ed25519). If there are, parsers SHOULD issue a warning.
Parers MAY reject the file, choose an arbitrary RelayLine, or ignore
both RelayLines.

If a parser does not recognize any extra material in a RelayLine,
the extra material MUST be ignored.

Each RelayLine includes the following KeyValue pairs:

"node_id" hexdigest

\[Exactly once.\]

The fingerprint for the relay's RSA identity key.

```text
      Note: In bandwidth files read by Tor versions earlier than
            0.3.4.1-alpha, node_id MUST NOT be at the end of the Line.
            These authority versions are no longer supported.
```

Current Tor versions ignore master_key_ed25519, so node_id MUST be
present in each relay Line.

Implementations of version 1.1.0 and later SHOULD include both node_id
and master_key_ed25519. Parsers SHOULD accept Lines that contain at
least one of them.

From version 1.9.0 of this specification, "node_id" does not longer need to
start with the dollar sign before the hexdigit.

"master_key_ed25519" MasterKey

\[Zero or one time.\]

The relays's master Ed25519 key, base64 encoded,
without trailing "="s, to avoid ambiguity with KeyValue "="
character.

This KeyValue pair SHOULD be present, see the note under node_id.

This KeyValue was added in version 1.1.0 of this specification.

"bw" Bandwidth

\[Exactly once.\]

The bandwidth of this relay in kilobytes per second.

No Zero Bandwidths:
Tor accepts zero bandwidths, but they trigger bugs in older Tor
implementations. Therefore, implementations SHOULD NOT produce zero
bandwidths. Instead, they SHOULD use one as their minimum bandwidth.
If there are zero bandwidths, the parser MAY ignore them.

Bandwidth Aggregation:
Multiple measurements can be aggregated using an averaging scheme,
such as a mean, median, or decaying average.

Bandwidth Scaling:
Torflow scales bandwidths to kilobytes per second. Other
implementations SHOULD use kilobytes per second for their initial
bandwidth scaling.

If different implementations or configurations are used in votes for
the same network, their measurements MAY need further scaling. See
Appendix B for information about scaling, and one possible scaling
method.

MaxAdvertisedBandwidth:
Bandwidth generators MUST limit the relays' measured bandwidth based
on the MaxAdvertisedBadwidth.
A relay's MaxAdvertisedBandwidth limits the bandwidth-avg in its
descriptor. bandwidth-avg is the minimum of MaxAdvertisedBandwidth,
BandwidthRate, RelayBandwidthRate, BandwidthBurst, and
RelayBandwidthBurst.
Therefore, generators MUST limit a relay's measured bandwidth to its
descriptor's bandwidth-avg. This limit needs to be implemented in the
generator, because generators may scale consensus weights before
sending them to Tor.
Generators SHOULD NOT limit measured bandwidths based on descriptors'
bandwidth-observed, because that penalises new relays.

sbws limits the relay's measured bandwidth to the bandwidth-avg
advertised.

Torflow partitions relays based on their bandwidth. For unmeasured
relays, Torflow uses the minimum of all descriptor bandwidths,
including bandwidth-avg (MaxAdvertisedBandwidth) and
bandwidth-observed. Then Torflow measures the relays in each partition
against each other, which implicitly limits a relay's measured
bandwidth to the bandwidths of similar relays.

Torflow also generates consensus weights based on the ratio between the
measured bandwidth and the minimum of all descriptor bandwidths (at the
time of the measurement). So when an operator reduces the
MaxAdvertisedBandwidth for a relay, Torflow reduces that relay's
measured bandwidth.

KeyValue

\[Zero or more times.\]

Future format versions may include additional KeyValue pairs on a
RelayLine.
Additional KeyValue pairs will be accompanied by a minor version
increment.

Implementations MAY add additional relay KeyValue pairs as needed.
This specification SHOULD be updated to avoid conflicting meanings
for the same Keywords.

Parsers MUST NOT rely on the order of these additional KeyValue
pairs.

Additional KeyValue pairs MUST NOT use any keywords specified in the
header format.
If there are, the parser MAY ignore conflicting keywords.
